import pandas as pd
from sklearn.preprocessing import MinMaxScaler

x1 = [1,2,3,4,5] 
x2 = [1,2,3,4,5] 
x3 = [1,2,3,4,5] 
df = pd.DataFrame({"x1": x1, "x2": x2, "x3": x3})

scaler = MinMaxScaler()
scaled_df = scaler.fit_transform(df)
scaled_df = pd.DataFrame(scaled_df, columns=['x1', 'x2', 'x3'])
print (scaled_df)
