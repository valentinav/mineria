import pandas as pd
import random
from KNN import KNN

class crossValidation:
    def __init__(self, dataset, numfolders):
        self.folder = numfolders
        self.dataset = dataset

    def DataFolders(self):
        file = [[] for i in range(self.folder)]
        
        self.dataset = (self.dataset).sort_values(by='Drug', ascending= True)
        data = (self.dataset).values.tolist()
        names = (self.dataset).columns.values.tolist()
        j=0
        while j < len(data):
            for i in range(len(file)):
                file[i].append(data[j])
                j += 1
        for i in range(len(file)):
            random.shuffle(file[i])
            df = pd.DataFrame(file[i], columns = names)
            file[i]=df

        return file

    def CrossValidation(self, k, folder):
        accuracy = 0
        for i in range(len(folder)):
            training,test = self.TrainingTest(folder,i)
            accuracy += self.AccuracyValue(training,k,test)
        return accuracy

    def TrainingTest(self,folder,test):
        dataTest = folder[test]
        if test == 0:
            dataTraining = pd.concat(folder[1:len(folder)])
            return dataTraining,dataTest
        dataTraining = folder[0:test]
        dataTraining = dataTraining + folder[test+1:len(folder)]
        dataTraining = pd.concat(dataTraining)
        return dataTraining,dataTest

    def getK(self, folder):
        Kaccuracy = list()
        for i in range(1,21):
            ac = self.CrossValidation(i,folder)
            ac = ac/200
            Kaccuracy.append([ac,i])
        Kaccuracy.sort(reverse=True)
        K = Kaccuracy[0][1]
        return K
            

    def AccuracyValue(self, training,k,test):
        vecinoIgualClase = 0
        prueba = test.values.tolist()
        for i in prueba:
            objKnn = KNN(training,k,i)
            if objKnn.KNNalgorithm() == i[6]:
                vecinoIgualClase += 1
        return vecinoIgualClase
            
