import math

class KNN:
    def __init__(self,D,K,t):
        self.data = D #training data
        self.K = K #number of neighbors closer
        self.t = t #input tuple to classify

    def KNNalgorithm(self):
        N = list() #Find set of neighbors for t
        dataset = (self.data).values.tolist()
        for i in range(len(dataset)):
            dato = (dataset[i]).copy()
            d = self.distance(dato)
            if len(N) < self.K:
                dato.insert(0,d)
                N.append(dato)
            else:
                N.sort()
                if (N[-1][0]) >= d:
                    dato.insert(0,d)
                    N.append(dato)
                    N.remove(N[-2]) #elimina el punúltimo elemento de la lista
        Nponderado = self.weigh(N)
        return Nponderado

    def distance(self, dato):
        tupla = self.t
        suma = (pow((dato[0]-tupla[0]),2)) + (pow((dato[1]-tupla[1]),2)) + (pow((dato[2]-tupla[2]),2)) + (pow((dato[3]-tupla[3]),2)) + (pow((dato[4]-tupla[4]),2)) + (pow((dato[5]-tupla[5]),2))     
        dist = math.sqrt(suma)

        return dist

    def isEmpty(self, structure):
        if structure:
            return False
        else:
            return True

    def weigh(self, lista):
        drugN = list()
        for i in lista:
            if i[7] not in drugN:
                drugN.append(i[7])

        ponderado = [[] for i in range(len(drugN))]

        for i in lista:
            pos = drugN.index(i[7])
            if self.isEmpty(ponderado[pos]):
                ponderado[pos] = (1/(pow(i[0],2)))
            else:
                suma = ponderado[pos]
                suma += (1/(pow(i[0],2)))
                ponderado[pos] = suma
        peso = list(zip(ponderado,drugN))
        peso.sort(key=lambda distancia: distancia[0])
        mayorPeso = peso[-1][1]
        return mayorPeso
        
        
            
        
            
            
            
        
        
        
