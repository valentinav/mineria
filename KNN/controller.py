import pandas as pd
from normalization import normalization
from crossValidation import crossValidation
from KNN import KNN
from newData import newData

class controller:
    def __init__(self):
        data = pd.read_csv("drugs.csv",header= 0, sep=";")
        n = normalization(data)
        self.dataset = n.run()
        
    def run(self):
        print("Información: el dataset cuenta con 200 registros")
        numfolders = int(input('Ingrese el número de folders para realizar la validación cruzada: '))
        while((200%numfolders)!= 0):
            numfolders = int(input("Por favor ingrese un número que de una división exacta: "))
            
        cv = crossValidation(self.dataset, numfolders)
        f = cv.DataFolders()
        print("**Obteniendo el mejor K a partir de validación cruzada**")
        K = cv.getK(f)
        print("Mejor K: ",K)
        t = newData()
        tupla = t.NewInfo()
        clasificacion = KNN(self.dataset, K, tupla)
        drug = clasificacion.KNNalgorithm()
        print("El medicamento para el paciente es: ", drug)        
        

a = controller()
a.run()
