import pandas as pd
from sklearn.preprocessing import MinMaxScaler

class normalization:

    def __init__(self, dataset):
        self.dataset = dataset

    def run(self):
        #discretize column Sex: 0F/1M
        sex = pd.get_dummies(self.dataset["Sex"],drop_first = True)

        #normalization numbers 
        age= self.dataset.loc[:,"Age"]
        na= self.dataset.loc[:,"Na"]
        k= self.dataset.loc[:,"K"]
        df = pd.DataFrame({"Age": age, "Na": na, "K": k})
        scaler = MinMaxScaler()
        scaled_df = scaler.fit_transform(df)
        scaled_df = pd.DataFrame(scaled_df, columns=['Age', 'Na', 'K'])

        #merge dataframes
        data = pd.merge(scaled_df, sex, left_index=True, right_index=True)

        #discretize columns BP, Cholesterol
        bp = []
        for i in range(len(self.dataset)):
            if self.dataset.loc[i,"BP"] == "HIGH":
                bp.append(1)
            if self.dataset.loc[i,"BP"] == "NORMAL":
                bp.append(0.5)
            if self.dataset.loc[i,"BP"] == "LOW":
                bp.append(0)
        ch= []
        for i in range(len(self.dataset)):
            if self.dataset.loc[i,"Cholesterol"] == "HIGH":
                ch.append(1)
            if self.dataset.loc[i,"Cholesterol"] == "NORMAL":
                ch.append(0.5)
            if self.dataset.loc[i,"Cholesterol"] == "LOW":
                ch.append(0)

        df = pd.DataFrame({"BP": bp, "Cholesterol": ch}, columns = ['BP', 'Cholesterol'])
        #merge dataframes
        drug = self.dataset.loc[:,"Drug"]
        data1 = pd.merge(data, df, left_index=True, right_index=True)
        data = pd.merge(data1, drug, left_index=True, right_index=True)

        return data
    
