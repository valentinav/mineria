#import pandas as pd

class newData:
    def __init__(self):
        self = self

    def normalizar(self, x, minimo, maximo):
        return ((x-minimo)/(maximo-minimo))

    def NewInfo(self):
        edad = int(input('Ingrese una edad entre 15 y 74: '))
        while (edad <15 or edad>74):
            print("Edad incorrecta")
            edad = int(input('Ingrese una edad entre 15 y 74: '))
        edad = self.normalizar(edad, 15, 74)

        na = float(input('Ingrese información de Sodio entre 0.5 y 0.89: '))
        while (na <0.5 or na>0.89):
            print("Información de Sodio incorrecta")
            na = float(input('Ingrese información de Sodio entre 0.5 y 0.89: '))
        na = self.normalizar(na, 0.5, 0.89)

        k = float(input('Ingrese información de Potasio entre 0.02 y 0.079: '))
        while(k<0.02 or k>0.079):
            print("Información de Potasio incorrecta")
            k = float(input('Ingrese información de Potasio entre 0.02 y 0.079: '))
        k = self.normalizar(k,0.02,0.079)

        m = input('Ingrese el género F o M: ')
        while(m!='F' and m!='M' and m!='f' and m!='m'):
            m = input("Por favor ingrese una de las siguientes opciones -> F o M: ")
        if (m == 'M' or m == 'm'):
            m = 1
        if (m == 'F' or m == 'f'):
            m = 0

        bp = input('Ingrese el BP -> HIGH, NORMAL, LOW: ')
        while(bp!='HIGH' and bp!='NORMAL' and bp!='LOW' and bp!='high' and bp!= 'normal' and bp!='low'):
            bp = input("Por favor ingrese una de las siguientes opciones para BP -> HIGH, NORMAL, LOW: ")
        if bp == 'HIGH' or bp == 'high':
            bp = 1
        if bp == 'NORMAL' or bp == 'normal':
            bp = 0.5
        if bp == "LOW" or bp == 'low':
            bp = 0

        ch = input('Ingrese el Cholesterol -> HIGH, NORMAL, LOW: ')
        while(ch!='HIGH' and ch!='NORMAL' and ch!='LOW' and ch!='high' and ch!= 'normal' and ch!='low'):
            ch = input("Por favor ingrese una de las siguientes opciones para Cholesterol -> HIGH, NORMAL, LOW: ")
        if ch == 'HIGH' or ch == 'high':
            ch = 1
        if ch == 'NORMAL' or ch == 'normal':
            ch = 0.5
        if ch == "LOW" or ch == 'low':
            ch = 0

        dato = [edad, na, k, m, bp, ch]
        #columns = ['Age','Na','K','Sex','BP','Cholesterol'])
        
        return dato

        
        
        
            

        
            
        
            
